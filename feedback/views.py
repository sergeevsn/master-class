from django.shortcuts import render
from .models import Post
from django.utils import timezone
from .forms import PostForm
from django.http import HttpResponse
from django.shortcuts import redirect
from django.shortcuts import render, get_object_or_404

# Create your views here.
def post_list(request):
    posts = Post.objects.order_by('-published_date')
    return render(request, 'feedback/post_list.html', {'posts': posts})

def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'feedback/thanks.html', {'post': post})

def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            post = form.save(commit=False)
            # post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect('thanks', pk=post.pk)
    else:
        form = PostForm()
    return render(request, 'feedback/post_edit.html', {'form': form})
