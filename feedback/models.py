from django.db import models
from django.utils import timezone


class Post(models.Model):
    ANIMALS = 'AN'
    FLOWERS = 'FL'
    BEE = 'BE'
    FISH = 'FI'
    CATEGORY_CHOICES = (
        (ANIMALS, 'Животноводство'),
        (FLOWERS, 'Растениеводство'),
        (BEE, 'Пчеловодство'),
        (FISH, 'Рыболовство'),
    )
    category = models.CharField(max_length=40,
                                      choices=CATEGORY_CHOICES)

    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, blank=True, null=True)
    fio = models.CharField(max_length=200)
    phone = models.CharField(max_length=13)
    email = models.EmailField()
    comment = models.TextField(max_length=600)

    created_date = models.DateTimeField(
            default=timezone.now)
    published_date = models.DateTimeField(
            blank=True, null=True)

    def publish(self):
        self.published_date = timezone.now()
        self.save()

    def __str__(self):
        return self.fio
